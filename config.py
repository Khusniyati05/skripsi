colors = {
    "primary": "#4041e9",
    "secondary": "#041A2F",
    "text": "#FFFFFF"
}

labels = {
    "dash": "Dashboard",
    "data": "Manajemen Dataset",
    "learn": "Model Learning",
    "res": "Hasil Learning",
    "pred": "Prediksi",
}


class IORedirector(object):
    '''A general class for redirecting I/O to this Text widget.'''

    def __init__(self, text_area):
        self.text_area = text_area


class StdoutRedirector(IORedirector):
    '''A class for redirecting stdout to this Text widget.'''

    def write(self, string):
        self.text_area.insert('end', string)
        self.text_area.see('end')
