from dbn import DBNRegressor, SupervisedDBNRegression
from tkinter.constants import ANCHOR, N
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from tkinter import *
from tkinter import messagebox
from math import ceil
from sklearn.model_selection import train_test_split, TimeSeriesSplit
from sklearn.preprocessing import minmax_scale
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, d2_absolute_error_score
from tkinter.ttk import Combobox
from pathlib import Path
from config import *
import sys
import json
from store import *
import pandas as pd
OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("../../assets")
STORE_PATH = OUTPUT_PATH / Path("../../store")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)


def relative_to_store(path: str) -> Path:
    return STORE_PATH / Path(path)


def config():
    with open(relative_to_assets("app.json")) as json_data_file:
        return json.load(json_data_file)


class Learning(Frame):
    def __init__(self, parent, controller=None, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.model = None

        self.data = {"name": StringVar(), "dataset_name": StringVar(),
                     "split": DoubleVar(value=0.1), "batch_size": IntVar(value=10), "rbm_stack": StringVar(value="4"), "lr_rbm": DoubleVar(value=0.115), "lr_bp": DoubleVar(value=0.85), "epoch": IntVar(value=10), "iterasi": IntVar(value=1000), "f_activation": StringVar(value="relu"), "hidden_neuron": StringVar(value="256, 256"), "solver": StringVar(value="adam"), "alpha": DoubleVar(value=1.0)}
        self.configure(bg=colors['secondary'])
        self.datasets = Exchange(name="datasets")
        self.canvas = Canvas(
            self,
            bg=colors['secondary'],
            height=432,
            width=797,
            bd=0,
            highlightthickness=0,
            relief="ridge",
        )

        self.canvas.place(x=0, y=0)
        self.canvas.create_rectangle(
            40.0, 14.0, 742.0, 16.0, fill="#EFEFEF", outline=""
        )

        self.form_field = [
            {
                "label": "Nama Model",
                "var": self.data["name"]
            },
            {
                "label": "Ukuran Data Test",
                "var": self.data["split"]
            },
            {
                "label": "Susunan Layer RBM",
                "var": self.data["rbm_stack"]
            },
            {
                "label": "Ukuran Batch",
                "var": self.data["batch_size"]
            },
            {
                "label": "Iterasi RBM",
                "var": self.data["epoch"]
            },
            {
                "label": "Susunan Layer Backpro",
                "var": self.data["hidden_neuron"]
            },
            {
                "label": "Alpha (Bias) Backpro",
                "var": self.data["alpha"]
            },
            {
                "label": "Max Iterasi Backpro",
                "var": self.data["iterasi"]
            },
        ]

        self.canvas.create_rectangle(
            372.0, 45.0, 374.0, 370.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_text(
            40.0,
            40.0,
            anchor="nw",
            text="Dataset",
            fill=colors["text"],
            font=("Montserrat Bold", 12 * -1),
        )
        self.select_dataset = Combobox(
            self,
            textvariable=self.data["dataset_name"],
            background="#EFEFEF",
            font=("Montserrat Bold", 10 * -1),
            foreground="#777777",
            state="readonly"
        )
        self.select_dataset.place(x=200.0, y=40.0, width=150.0, height=16.0)

        self.canvas.create_text(
            40.0,
            70.0 + (len(self.form_field) * 30),
            anchor="nw",
            text="Learning Rate RBM & BP",
            fill=colors["text"],
            font=("Montserrat Bold", 12 * -1),
        )
        lr_rbm = Entry(
            self,
            textvariable=self.data["lr_rbm"],
            bd=0,
            bg="#EFEFEF",
            highlightthickness=0,
            font=("Montserrat Bold", 12 * -1),
        )
        lr_rbm.place(x=200.0, y=70.0 + (len(self.form_field)
                                        * 30), width=70.0, height=16.0)
        lr_bp = Entry(
            self,
            textvariable=self.data["lr_bp"],
            bd=0,
            bg="#EFEFEF",
            highlightthickness=0,
            font=("Montserrat Bold", 12 * -1),
        )
        lr_bp.place(x=280.0, y=70.0 + (len(self.form_field)
                                       * 30), width=70.0, height=16.0)

        self.canvas.create_text(
            40.0,
            100.0 + (len(self.form_field) * 30),
            anchor="nw",
            text="Fungsi Optimasi Bobot",
            fill=colors["text"],
            font=("Montserrat Bold", 12 * -1),
        )
        self.select_solver = Combobox(
            self,
            textvariable=self.data["solver"],
            background="#EFEFEF",
            font=("Montserrat Bold", 10 * -1),
            foreground="#777777",
            state="readonly",
            values=["adam", "sgd", "lbfgs"]
        )
        self.select_solver.place(x=200.0, y=100.0 + (len(self.form_field)
                                                     * 30), width=150.0, height=16.0)

        self.canvas.create_text(
            40.0,
            130.0 + (len(self.form_field) * 30),
            anchor="nw",
            text="Fungsi Aktivasi",
            fill=colors["text"],
            font=("Montserrat Bold", 12 * -1),
        )
        self.select_factv = Combobox(
            self,
            textvariable=self.data["f_activation"],
            background="#EFEFEF",
            font=("Montserrat Bold", 10 * -1),
            foreground="#777777",
            state="readonly",
            values=["relu", "identity", "tanh", "logistic"]
        )
        self.select_factv.place(x=200.0, y=130.0 + (len(self.form_field)
                                                    * 30), width=150.0, height=16.0)

        self.addFormField()

        self.button_start_image = PhotoImage(
            file=relative_to_assets("btn_start.png"))
        self.button_start = Button(
            self,
            image=self.button_start_image,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_start,
            relief="flat",
            cursor='hand2', activebackground=colors['secondary'],
            background=colors['secondary'],
        )
        self.button_start.place(x=450.0, y=40.0, width=99.0, height=25.0)
        self.button_save_image = PhotoImage(
            file=relative_to_assets("btnsm_save.png"))
        self.button_save = Button(
            self,
            state="disabled",
            image=self.button_save_image,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_save,
            relief="flat",
            cursor='hand2', activebackground=colors['secondary'],
            background=colors['secondary'],
        )
        self.button_save.place(x=565.0, y=40.0, width=99.0, height=25.0)

        self.debug = Text(self, wrap='word',
                          font=("Montserrat Bold", 8 * -1))
        self.debug.place(x=400.0, y=85.0, width=330.0, height=300.0)

        self.y_test = list()
        self.y_pred = list()
        self.score = list()

    def addFormField(self):
        i = 0
        for field in self.form_field:
            self.canvas.create_text(
                40.0,
                70.0 + (i * 30),
                anchor="nw",
                text=field['label'],
                fill=colors["text"],
                font=("Montserrat Bold", 12 * -1),
            )
            entry_name = Entry(
                self,
                textvariable=field['var'],
                bd=0,
                bg="#EFEFEF",
                highlightthickness=0,
                font=("Montserrat Bold", 12 * -1),
            )
            entry_name.place(x=200.0, y=70.0 + (i * 30),
                             width=150.0, height=16.0)
            i = i + 1

    def handle_start(self):
        # check if any fields are empty
        sys.stdout = StdoutRedirector(self.debug)
        self.y_test = list()
        self.y_pred = list()
        self.score = list()
        self.button_start["state"] = "disabled"
        self.debug.delete(1.0, END)

        for val in self.data.values():
            if val.get() == "":
                messagebox.showinfo(
                    "Error", "Silahkan isi semua input terlebih dahulu")
                self.button_start["state"] = "normal"
                return
        self.model = DBNRegressor(
            rbm_stack=[int(i) for i in self.data['rbm_stack'].get().split(",")], batch_size=self.data['batch_size'].get(), learning_rate_rbm=self.data['lr_rbm'].get(), n_iter_rbm=self.data['epoch'].get(), bp_hidden_layer=[int(i) for i in self.data['hidden_neuron'].get().split(",")], activation_function=self.data['f_activation'].get(), optimization_algorithm=self.data['solver'].get(), alpha=self.data['alpha'].get(), learning_rate_bp=self.data['lr_bp'].get(), max_iter_bp=self.data['iterasi'].get())
        select = next(
            (item for item in self.datasets.getAll() if item['name'] == self.select_dataset.get()), None)
        dataset = pd.read_csv(select['path'], index_col="Tanggal",
                              parse_dates=["Tanggal"], dayfirst=True)
        X = dataset[["kurs", "minyak"]].values.astype(float)
        y = dataset["emas"].values.astype(float)
        X = minmax_scale(X)
        X = self.model.transform(X, y)

        if self.data["split"].get() < 1:
            X_train, X_test, y_train, y_test = train_test_split(
                X, y, test_size=self.data["split"].get(), shuffle=False)
            self.model.fit(X_train, y_train)

            # Test
            Y_pred = self.model.predict(X_test)
            print('\n\nDone.')
            print(f'R-squared: {r2_score(y_test, Y_pred)}')
            print(f'MAE: {mean_absolute_error(y_test, Y_pred)}')
            print(
                f'RMSE: {mean_squared_error(y_test, Y_pred,squared=False)}')
            print(
                f'MAPE: {mean_absolute_percentage_error(y_test, Y_pred)}')

            self.y_test.append(y_test.tolist())
            self.y_pred.append(Y_pred.tolist())
            self.score.append({
                'R-squared': r2_score(y_test, Y_pred),
                'MAE': mean_absolute_error(y_test, Y_pred),
                'RMSE': mean_squared_error(y_test, Y_pred, squared=False),
                'MAPE': mean_absolute_percentage_error(y_test, Y_pred),
            })
        else:
            ts_cv = TimeSeriesSplit(
                n_splits=ceil(self.data["split"].get()),
            )

            all_splits = list(ts_cv.split(X, y))
            fold = 1
            for train_index, test_index in all_splits:
                X_train = X[train_index]
                y_train = y[train_index]
                X_test = X[test_index]
                y_test = y[test_index]
                print(f'Fold ke {fold}')
                self.debug.insert('insert', f'\nSplit ke {fold}')
                fold += 1

                # Data scaling
                self.model.fit(X_train, y_train)

                # Test
                Y_pred = self.model.predict(X_test)
                print('\n\nDone.')
                self.debug.insert('insert', f'\n\nDone.')

                print(f'R-squared: {r2_score(y_test, Y_pred)}')
                print(f'MAE: {mean_absolute_error(y_test, Y_pred)}')
                print(
                    f'RMSE: {mean_squared_error(y_test, Y_pred,squared=False)}')
                print(
                    f'MAPE: {mean_absolute_percentage_error(y_test, Y_pred)}')

                self.y_test.append(y_test.tolist())
                self.y_pred.append(Y_pred.tolist())
                self.score.append({
                    'R-squared': r2_score(y_test, Y_pred),
                    'MAE': mean_absolute_error(y_test, Y_pred),
                    'RMSE': mean_squared_error(y_test, Y_pred, squared=False),
                    'MAPE': mean_absolute_percentage_error(y_test, Y_pred),
                })
        self.button_save['state'] = "normal"
        self.button_start["state"] = "normal"
        sys.stdout = sys.__stdout__

    def handle_save(self):
        self.debug.delete(1.0, END)
        model = dict()
        for k in self.data:
            model[k] = self.data[k].get()
        model['y_test'] = self.y_test
        model['y_pred'] = self.y_pred
        model['score'] = self.score

        self.models = Exchange(name='models')
        self.models.insert(model)

        self.model.save(relative_to_store(model['name'] + ".pkl"))

        messagebox.showinfo("Success", "Model baru berhasil ditambahkan")
        # clear all fields
        for label in self.data.keys():
            self.data[label].set("")
        self.button_start['state'] = 'normal'
        self.button_save['state'] = 'disabled'

    def pageData(self):
        self.debug.delete(1.0, END)
        t = ()
        for item in self.datasets.getAll():
            add = (item["name"],)
            t = t + add
        self.select_dataset["values"] = t
