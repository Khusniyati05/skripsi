from tkinter.constants import ANCHOR, N
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from tkinter import Frame, Canvas, Entry, PhotoImage, N
from pathlib import Path
from config import *
import json
from store import Exchange
OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("../../assets")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)


class Dashboard(Frame):
    def __init__(self, parent, controller=None, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        self.configure(bg=colors["secondary"])

        self.canvas = Canvas(
            self,
            bg=colors["secondary"],
            height=432,
            width=797,
            bd=0,
            highlightthickness=0,
            relief="ridge",
        )

        self.canvas.place(x=0, y=0)
        self.canvas.create_rectangle(
            40.0, 14.0, 742.0, 16.0, fill="#EFEFEF", outline=""
        )
        self.canvas.entry_image_1 = PhotoImage(
            file=relative_to_assets("entry_1.png"))
        entry_bg_1 = self.canvas.create_image(
            115.0, 81.0, image=self.canvas.entry_image_1)

        self.canvas.create_text(
            56.0,
            45.0,
            anchor="nw",
            text="Dataset",
            fill=colors["secondary"],
            font=("Montserrat Bold", 14 * -1),
        )

        # Vacant Text
        self.dataset_view = self.canvas.create_text(
            164.0,
            63.0,
            anchor="ne",
            fill=colors["secondary"],
            font=("Montserrat Bold", 48 * -1),
            justify="right",
        )

        self.canvas.entry_image_2 = PhotoImage(
            file=relative_to_assets("entry_2.png"))
        entry_bg_2 = self.canvas.create_image(
            299.0, 81.0, image=self.canvas.entry_image_2)

        self.canvas.create_text(
            240.0,
            45.0,
            anchor="nw",
            text="Model",
            fill=colors["secondary"],
            font=("Montserrat Bold", 14 * -1),
        )

        self.model_view = self.canvas.create_text(
            346.0,
            63.0,
            anchor="ne",
            fill=colors["secondary"],
            font=("Montserrat Bold", 48 * -1),
            justify="right",
        )

    def pageData(self):
        datasets = Exchange(name="datasets")
        models = Exchange(name="models")
        self.canvas.itemconfigure(self.dataset_view, text=datasets.total())
        self.canvas.itemconfigure(self.model_view, text=models.total())
