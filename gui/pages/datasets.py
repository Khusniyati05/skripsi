from tkinter.constants import ANCHOR, N
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from tkinter import (
    Frame,
    Canvas,
    Entry,
    Text,
    Button,
    PhotoImage,
    messagebox,
    StringVar,
)
from tkinter.ttk import Treeview
from tkinter.ttk import Combobox
from tkinter.filedialog import askopenfilename
from pathlib import Path
from config import *
from store import Exchange
import pandas as pd
OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("../../assets")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)


class Datasets(Frame):
    def __init__(self, parent, controller=None, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.selected_dataset = StringVar()
        self.data = {"path": StringVar(),
                     "name": StringVar()}
        self.datasets = Exchange(name="datasets")

        self.configure(bg=colors['secondary'])

        self.canvas = Canvas(
            self,
            bg=colors['secondary'],
            height=432,
            width=797,
            bd=0,
            highlightthickness=0,
            relief="ridge",
        )

        self.canvas.place(x=0, y=0)
        self.canvas.create_rectangle(
            40.0, 14.0, 742.0, 16.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_text(
            40.0,
            33.0,
            anchor="nw",
            text="List Dataset",
            fill=colors['text'],
            font=("Montserrat Bold", 26 * -1),
        )

        self.select_dataset = Combobox(
            self,
            textvariable=self.selected_dataset,
            background="#EFEFEF",
            font=("Montserrat Bold", 18 * -1),
            foreground="#777777",
            state="readonly"
        )
        self.select_dataset.place(x=40.0, y=85.0, width=400.0, height=30.0)
        self.select_dataset.bind('<<ComboboxSelected>>', self.handle_treeview)

        self.columns = {
            "Tanggal": ["Tanggal", 120],
            "Harga Minyak": ["Harga Minyak", 80],
            "Kurs": ["Kurs", 80],
            "Harga Emas": ["Harga Emas", 80],
        }

        self.treeview = Treeview(
            self,
            show="headings",
            height=200,
            selectmode="browse",
            # ="#FFFFFF",
            # fg="#5E95FF",
            # font=("Montserrat Bold", 18 * -1)
        )

        # Show the headings
        # for idx, txt in self.columns.items():
        #     self.treeview.heading(idx, text=txt[0])
        #     # Set the column widths
        #     self.treeview.column(idx, width=txt[1])

        self.treeview.place(x=40.0, y=140.0, width=400.0, height=229.0)

        self.canvas.create_rectangle(
            470.0, 59.0, 472.0, 370.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_text(
            520.0,
            58.0,
            anchor="nw",
            text="Tambah Dataset Baru",
            fill=colors['text'],
            font=("Montserrat Bold", 16 * -1),
        )

        self.canvas.create_text(
            500.0,
            108.0,
            anchor="nw",
            text="Nama Dataset",
            fill=colors["text"],
            font=("Montserrat Bold", 14 * -1),
        )
        entry_1 = Entry(
            self,
            textvariable=self.data["name"],
            bd=0,
            bg="#EFEFEF",
            highlightthickness=0,
            font=("Montserrat Bold", 14 * -1),
        )
        entry_1.place(x=500.0, y=145.0, width=240.0, height=22.0)

        self.canvas.create_text(
            500.0,
            188.0,
            anchor="nw",
            text="File",
            fill=colors["text"],
            font=("Montserrat Bold", 14 * -1),
        )
        self.entry_path = Entry(
            self,
            state="readonly",
            textvariable=self.data["path"],
            bd=0,
            bg="#EFEFEF",
            highlightthickness=0,
            font=("Montserrat Bold", 10 * -1),
        )
        self.entry_path.place(x=500.0, y=225.0, width=210.0, height=22.0)

        button_file = Button(
            self,
            text="...",
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_file,
            relief="flat",
        )
        button_file.place(x=720.0, y=225.0, width=22.0, height=22.0)

        self.button_save_image = PhotoImage(
            file=relative_to_assets("button_save.png"))
        button_save = Button(
            self,
            image=self.button_save_image,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_save,
            relief="flat",
            cursor='hand2', activebackground=colors['secondary'],
            background=colors['secondary'],
        )
        button_save.place(x=530.0, y=285.0, width=190.0, height=48.0)

        # Insert data

        # Add selection event
        # self.treeview.bind("<<TreeviewSelect>>", self.on_treeview_select)

    def handle_refresh(self):
        t = ()
        for item in self.datasets.getAll():
            add = (item["name"],)
            t = t + add
        self.select_dataset["values"] = t

    def handle_file(self):
        self.data['path'].set(askopenfilename(
            filetypes=[("data file", ".csv")]))

    def handle_save(self):
        # check if any fields are empty
        for val in self.data.values():
            if val.get() == "":
                messagebox.showinfo(
                    "Error", "Silahkan isi semua input terlebih dahulu")
                return

        # Save the room
        self.datasets.insert({
            "name": self.data["name"].get(),
            "path": self.data["path"].get(),
        })

        messagebox.showinfo("Success", "Dataset baru berhasil ditambahkan")
        # clear all fields
        for label in self.data.keys():
            self.data[label].set("")

        self.handle_refresh()

    def handle_treeview(self, event):
        select = next(
            (item for item in self.datasets.getAll() if item['name'] == self.select_dataset.get()), None)
        df = pd.read_csv(select['path'])
        df_list = list(df.columns.values)
        df_rset = df.to_numpy().tolist()
        self.treeview['columns'] = df_list
        for h in df_list:
            self.treeview.heading(h, text=h)
            self.treeview.column(h, width=80)
        self.treeview.delete(*self.treeview.get_children())
        for dt in df_rset:
            v = [r for r in dt]
            self.treeview.insert('', 'end', iid=v[0], values=v)

    def pageData(self):
        self.handle_refresh()
