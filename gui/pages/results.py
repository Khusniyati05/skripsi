from store import *
from config import *
import json
from pathlib import Path
from tkinter.ttk import Combobox
from tkinter import (
    Frame,
    Canvas,
    Entry,
    Text,
    Button,
    PhotoImage,
    messagebox,
    StringVar,
    IntVar,
    DoubleVar,
    scrolledtext,
    END
)
import pandas as pd
import numpy as np
import datetime
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from tkinter.constants import ANCHOR, N
import matplotlib
matplotlib.rc('xtick', labelsize=6)
matplotlib.rc('ytick', labelsize=6)
matplotlib.rc('figure', labelsize=8)
matplotlib.rc('legend', fontsize=6)  # using a size in points
matplotlib.use("TkAgg")
OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("../../assets")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)


def config():
    with open(relative_to_assets("app.json")) as json_data_file:
        return json.load(json_data_file)


class Results(Frame):
    def __init__(self, parent, controller=None, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        self.configure(bg=colors['secondary'])
        self.data = {"name": StringVar(), "dataset_name": StringVar(),
                     "split": DoubleVar(), "batch_size": IntVar(), "rbm_stack": StringVar(), "lr_rbm": DoubleVar(), "lr_bp": DoubleVar(), "epoch": IntVar(), "iterasi": IntVar(), "f_activation": StringVar(), "hidden_neuron": StringVar(), "solver": StringVar(), "alpha": DoubleVar()}
        self.score = {"MAE": StringVar(), "RMSE": StringVar(
        ), "MAPE": StringVar()}

        self.selected_model = StringVar()
        self.selected_split = IntVar()
        self.canvas = Canvas(
            self,
            bg=colors['secondary'],
            height=432,
            width=797,
            bd=0,
            highlightthickness=0,
            relief="ridge",
        )

        self.models = Exchange(name="models")

        self.canvas.place(x=0, y=0)
        self.canvas.create_rectangle(
            40.0, 14.0, 742.0, 16.0, fill="#EFEFEF", outline=""
        )

        self.select_models = Combobox(
            self,
            textvariable=self.selected_model,
            background="#EFEFEF",
            font=("Montserrat Bold", 10 * -1),
            foreground="#777777",
            state="readonly"
        )
        self.select_models.place(x=40.0, y=40.0, width=150.0, height=16.0)
        self.select_models.bind('<<ComboboxSelected>>',
                                self.handle_select_models)

        self.select_split = Combobox(
            self,
            textvariable=self.selected_split,
            background="#EFEFEF",
            font=("Montserrat Bold", 10 * -1),
            foreground="#777777",
            state="readonly"
        )
        self.select_split.place(x=200.0, y=40.0, width=150.0, height=16.0)
        self.select_split.bind('<<ComboboxSelected>>',
                               self.handle_select_split)

        self.form_field = [
            {
                "label": "Nama Model",
                "var": self.data["name"]
            },
            {
                "label": "Nama Dataset",
                "var": self.data["dataset_name"]
            },
            {
                "label": "Ukuran Data Test",
                "var": self.data["split"]
            },
            {
                "label": "Susunan Layer RBM",
                "var": self.data["rbm_stack"]
            },
            {
                "label": "Ukuran Batch",
                "var": self.data["batch_size"]
            },
            {
                "label": "Iterasi RBM",
                "var": self.data["epoch"]
            },
            {
                "label": "Susunan Layer Backpro",
                "var": self.data["hidden_neuron"]
            },
            {
                "label": "Alpha (Bias) Backpro",
                "var": self.data["alpha"]
            },
            {
                "label": "Max Iterasi Backpro",
                "var": self.data["iterasi"]
            },
            {
                "label": "Fungsi Optimasi Bobot",
                "var": self.data["solver"]
            },
            {
                "label": "Fungsi Aktivasi",
                "var": self.data["f_activation"]
            },
        ]
        self.score_field = [
            {
                "label": "MAE",
                "var": self.score["MAE"]
            },
            {
                "label": "RMSE",
                "var": self.score["RMSE"]
            },
            {
                "label": "MAPE",
                "var": self.score["MAPE"]
            },
        ]

        self.canvas.create_rectangle(
            372.0, 45.0, 374.0, 370.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_text(
            40.0,
            70.0 + (len(self.form_field) * 30),
            anchor="nw",
            text="Learning Rate RBM & BP",
            fill=colors["text"],
            font=("Montserrat Bold", 12 * -1),
        )
        lr_rbm = Entry(
            self,
            textvariable=self.data["lr_rbm"],
            bd=0,
            bg="#EFEFEF",
            highlightthickness=0,
            state="readonly",
            font=("Montserrat Bold", 12 * -1),
        )
        lr_rbm.place(x=200.0, y=70.0 + (len(self.form_field)
                                        * 30), width=70.0, height=16.0)
        lr_bp = Entry(
            self,
            textvariable=self.data["lr_bp"],
            bd=0,
            bg="#EFEFEF",
            highlightthickness=0,
            state="readonly",
            font=("Montserrat Bold", 12 * -1),
        )
        lr_bp.place(x=280.0, y=70.0 + (len(self.form_field)
                                       * 30), width=70.0, height=16.0)

        self.addFormField()
        self.addScoreField()

        self.plot_figure = Figure(figsize=(3.3, 2), dpi=100)
        self.plot_figure.clear()
        self.plot_canvas = FigureCanvasTkAgg(self.plot_figure, self.canvas)
        self.plot_canvas.draw()
        self.plot_canvas.get_tk_widget().place(x=400.0, y=190.0)

    def addFormField(self):
        i = 0
        for field in self.form_field:
            self.canvas.create_text(
                40.0,
                70.0 + (i * 30),
                anchor="nw",
                text=field['label'],
                fill=colors["text"],
                font=("Montserrat Bold", 12 * -1),
            )
            entry_name = Entry(
                self,
                textvariable=field['var'],
                bd=0,
                bg="#EFEFEF",
                highlightthickness=0,
                font=("Montserrat Bold", 12 * -1),
                state="readonly"
            )
            entry_name.place(x=200.0, y=70.0 + (i * 30),
                             width=150.0, height=16.0)
            i = i + 1

    def addScoreField(self):
        i = 0
        for field in self.score_field:
            self.canvas.create_text(
                400.0,
                40.0 + (i * 30),
                anchor="nw",
                text=field['label'],
                fill=colors["text"],
                font=("Montserrat Bold", 12 * -1),
            )
            entry_name = Entry(
                self,
                textvariable=field['var'],
                bd=0,
                bg="#EFEFEF",
                highlightthickness=0,
                font=("Montserrat Bold", 12 * -1),
                state="readonly"
            )
            entry_name.place(x=560.0, y=40.0 + (i * 30),
                             width=150.0, height=16.0)
            i = i + 1

    def handle_refresh(self):
        for label in self.data.keys():
            self.data[label].set("")
        for label in self.score.keys():
            self.score[label].set("")
        t = ()
        for item in self.models.getAll():
            add = (item["name"],)
            t = t + add
        self.select_models["values"] = t
        self.plot_figure.clear()

    def handle_select_models(self, event):
        select = next(
            (item for item in self.models.getAll() if item['name'] == self.select_models.get()), None)
        t = ()
        for i in range(len(select['score'])):
            add = (i + 1,)
            t = t + add
        # print(idx)
        self.select_split["values"] = t

    def handle_select_split(self, event):
        model = self.select_models.get()
        index = self.selected_split.get() - 1
        select = next(
            (item for item in self.models.getAll() if item['name'] == model), None)
        for k in self.data:
            self.data[k].set(select[k])
        for k in self.score:
            self.score[k].set(
                select["score"][index][k])
        dataset = next(
            (item for item in Exchange(name="datasets").getAll() if item['name'] == select["dataset_name"]), None)
        self.plot_figure.clear()
        plt = self.plot_figure.add_subplot()
        plt.plot()
        dataframe = pd.read_csv(dataset["path"], parse_dates=[
                                "Tanggal"], dayfirst=True)

        # Plotting the time series of given dataframe
        plt.set_title("Hasil Prediksi", fontdict={"fontsize": 8})
        plt.plot(select["y_test"][index], label="Aktual")
        plt.plot(select["y_pred"][index], label="Prediksi")
        self.plot_figure.autofmt_xdate()
        self.plot_figure.legend(loc="upper left")
        self.plot_canvas.draw()
        # print(dataframe["Tanggal"][[1, 2]])

    def pageData(self):
        self.handle_refresh()
