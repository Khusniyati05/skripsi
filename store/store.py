from pathlib import Path
from config import *
import json

OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("./")


def relative_folder(path: str) -> Path:
    return ASSETS_PATH / Path(path)


class Exchange():
    def __init__(self, *args, **kwargs):
        self.store_name = kwargs["name"]
        self.items = self.getAll()

    def getAll(self) -> list:
        with open(relative_folder(self.store_name + ".json")) as json_data_file:
            return json.load(json_data_file)

    def insert(self, data):
        self.items.append(data)
        with open(relative_folder(self.store_name + ".json"), "w") as outfile:
            json.dump(self.items, outfile)

    def total(self) -> int:
        return self.items.__len__()
