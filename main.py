from gui import *
import tkinter as tk
import numpy as np
from store import *

root = tk.Tk()  # Make temporary window for app to start
root.withdraw()  # WithDraw the window


if __name__ == "__main__":
    app()
    root.mainloop()
